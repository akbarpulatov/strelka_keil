#include "main.h"
#include "Functions.h"

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)	//External Interrupt Function
{
	if (GPIO_Pin == CS_pin)		//Interrupt from CS pin, (Falling Edge)
		{
			if (!Ser_CS)		//Beginning of the message, default values should be set
				{
					count = 0;
					tempmessage = 0;
				}
		}
	else if (GPIO_Pin == CLK_pin)	//Interrupt from CLK pin, NOT(Falling Edge) was set.
		{
			if (!Ser_CS)
			{
				tempmessage |= ((Ser_DI) & 0x01) << count++;
			}
			
		}
	
	if (count == 8)	// Agar hamma bitlar kelgan va opredelyonniy shartlar bajarilgan bo`lsa messageni yangilaymiz
		{
			//if (CheckCRC(tempmessage))
			//if(Ser_Check(tempmessage))
//			{
				my_Message = tempmessage;
//				SerMessage = (BYTE)tempmessage;
//			}
			count = 0;
		}
	if(my_Message != 85)
		{
			LEDG = 0;
		}
}

BYTE CheckCRC(WORD checkingmess)
{
	CRC_HandleTypeDef   CrcHandle;
	/*##-1- Configure the CRC peripheral #######################################*/
	CrcHandle.Instance = CRC;

	if (HAL_CRC_Init(&CrcHandle) != HAL_OK)
	{
		/* Initialization Error */
		Error_Handler();
	}

	/*##-2- Compute the CRC of "aDataBuffer" ###################################*/
	uwCRCValue = HAL_CRC_Accumulate(&CrcHandle, (uint32_t *)aDataBuffer, BUFFER_SIZE);

	/*##-3- Compare the CRC value to the Expected one ##########################*/
	if (uwCRCValue != uwExpectedCRCValue)
	{
		/* Wrong CRC value: enter Error_Handler */
		Error_Handler();
	}
	else
	{
		/* Right CRC value: Turn LED2 on */
		return true;
	}
	return false;
}

//============================== < Led Matrix States > ======================================================


