/*
 *	File:		akbardeff.h
 *	Author:		Akbar Pulatov
 *	Created on 25.09.2019, 17:16
 *	
 *	*/

#ifndef __AKBARDEFF_H
#define __AKBARDEFF_H

//======= �������� =====================================================================================================

//======================================================================================================================
//typedef uint8_t				 					uchar;
//typedef unsigned int						uint;

typedef unsigned char			uint8_t;
typedef uint8_t                 u8;   		// 8-bit unsigned
//typedef uint16_t                u16;		// 8-bit unsigned
//typedef uint32_t                u32;		// 8-bit unsigned

typedef unsigned char           BYTE;   		// 8-bit unsigned
typedef unsigned short int      WORD;   		// 16-bit unsigned
typedef unsigned int            UINT;   		// 32-bit unsigned
typedef unsigned long           DWORD;   		// 32-bit unsigned
typedef unsigned long long      QWORD;   		// 64-bit unsigned
typedef signed char             CHAR;   		// 8-bit signed
typedef signed short int        SHORT;   		// 16-bit signed
typedef signed long             LONG;   		// 32-bit signed
typedef signed long long        LONGLONG;   	// 64-bit signed



#define bool _Bool
#define BOOL bool	
#define true 1
#define false 0
//======================================================================================================================

//======================================================================================================================
#if defined(__ICCARM__) 
#define Nop() asm("NOP")
#elif defined(__CC_ARM)
 #define Nop() __nop()
#else
#error "���������������� ����������"
#endif
#define Reset()		NVIC_SystemReset()
//======================================================================================================================
#define BITBAND(addr, bitnum) ((addr & 0xF0000000)+0x2000000+((addr &0xFFFFF)<<5)+(bitnum<<2)) 
#define MEM_ADDR(addr)  *((volatile unsigned long  *)(addr)) 
#define BIT_ADDR(addr, bitnum)   MEM_ADDR(BITBAND(addr, bitnum)) 
//----------------------------------------------------------------------------------------------------------------------
#define GPIOA_ODR_Addr    (GPIOA_BASE+12) //0x4001080C 
#define GPIOB_ODR_Addr    (GPIOB_BASE+12) //0x40010C0C 
#define GPIOC_ODR_Addr    (GPIOC_BASE+12) //0x4001100C 
#define GPIOD_ODR_Addr    (GPIOD_BASE+12) //0x4001140C 
#define GPIOE_ODR_Addr    (GPIOE_BASE+12) //0x4001180C 
#define GPIOF_ODR_Addr    (GPIOF_BASE+12) //0x40011A0C    
#define GPIOG_ODR_Addr    (GPIOG_BASE+12) //0x40011E0C    

#define GPIOA_IDR_Addr    (GPIOA_BASE+8) //0x40010808 
#define GPIOB_IDR_Addr    (GPIOB_BASE+8) //0x40010C08 
#define GPIOC_IDR_Addr    (GPIOC_BASE+8) //0x40011008 
#define GPIOD_IDR_Addr    (GPIOD_BASE+8) //0x40011408 
#define GPIOE_IDR_Addr    (GPIOE_BASE+8) //0x40011808 
#define GPIOF_IDR_Addr    (GPIOF_BASE+8) //0x40011A08 
#define GPIOG_IDR_Addr    (GPIOG_BASE+8) //0x40011E08 
//----------------------------------------------------------------------------------------------------------------------
#define PAout(n)   BIT_ADDR(GPIOA_ODR_Addr,n)  //
#define PAin(n)    BIT_ADDR(GPIOA_IDR_Addr,n)  //

#define PBout(n)   BIT_ADDR(GPIOB_ODR_Addr,n)  //
#define PBin(n)    BIT_ADDR(GPIOB_IDR_Addr,n)  //

#define PCout(n)   BIT_ADDR(GPIOC_ODR_Addr,n)  //
#define PCin(n)    BIT_ADDR(GPIOC_IDR_Addr,n)  //

#define PDout(n)   BIT_ADDR(GPIOD_ODR_Addr,n)  //
#define PDin(n)    BIT_ADDR(GPIOD_IDR_Addr,n)  //

#define PEout(n)   BIT_ADDR(GPIOE_ODR_Addr,n)  //
#define PEin(n)    BIT_ADDR(GPIOE_IDR_Addr,n)  //

#define PFout(n)   BIT_ADDR(GPIOF_ODR_Addr,n)  //
#define PFin(n)    BIT_ADDR(GPIOF_IDR_Addr,n)  //

#define PGout(n)   BIT_ADDR(GPIOG_ODR_Addr,n)  //
#define PGin(n)    BIT_ADDR(GPIOG_IDR_Addr,n)  //
//======================================================================================================================
//#define V25						1750
//#define Avg_Slope				5
//=========================== ���������� ������� =======================================================================

//=========================== ���������� ������������ ==================================================================
#define LEDG					PCout(13)

#define DS_595					PAout(5)					
#define SH_595					PAout(7)
#define ST_595					PAout(6)

#define IN_1					PBin(0)		// cpu_in1
#define IN_2					PBin(1)		// cpu_in2
#define IN_3					PBin(10)	// cpu_in3
#define IN_4					PBin(11)	// cpu_in4
 
#define	Undefined				!IN_1
#define	Ser_DI					!IN_2					
#define	Ser_CLK					!IN_3
#define	Ser_CS					IN_4

#define CLK_pin					GPIO_PIN_10
#define CS_pin					GPIO_PIN_11
 
////======================================================================================================================
//#define	ClockAnim				ErrFlags.b0
//#define	Err1					ErrFlags.b1
//#define	Err2					ErrFlags.b2
//======================================================================================================================
#define DelayNop10() 			Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();Nop();
#define DelShift 				msTMR1
#define DelKrStr 				msTMR2
#define DelRstr 				msTMR3
#define DelRotate 			msTMR4
#define DelDemo 				msTMR5

#define Del_BlinkKrest			msTMR11
#define Del_BlinkError			msTMR12
#define DelNumber				msTMR13
#define DelPeriodCheck			msTMR14
#define DelBlinkConnErr			msTMR15
#define Timer100ms				msTMR16
#define Timer500ms				msTMR17


//***************** TIME CONSTANTS ************************************************************************************/
#define _DelPeriodCheck			60000
#define _DelInitCheck			10000

//======================================================================================================================	
/*************** ����� ************************************************************************************************/
#define	Tick                Flags.b0
#define SerCheck			Flags.b1
#define FlagRead			Flags.b2
#define InStrelka			Flags.b3

#define FlagKrest			Flags.b4
#define FlagError			Flags.b5
#define FlagStrelka			Flags.b6
#define FlagNumber			Flags.b7
#define FlagConnError		Flags.b8
#define FlagNewMessage		tempflag

#define MaskErrorBits		0x0F

//============< States of LED Matrix >==========================================
#define _StateDispStrelka		0x7B
#define _StateDispKrest			0x7C
#define _StateDispHourGlass		0x7D
#define _StateDispConnErr		0x7E

#define _IgnoreMessage			0x7F

//#define _StateDispError			0x13

WORD ReadMessage(BYTE);
BYTE Ser_Check(BYTE);
void TestOfFunctions(void);


void SysTickInit(WORD frequency);
void R_G_ToLed(void);
void ShiftLed(BYTE line);
BYTE  Rev8To8(SHORT  data);
void DelayNop(void);
WORD T_Get_Adc(BYTE ch);
WORD T_Get_Temp(void);
WORD T_Get_Adc_Average(BYTE ch, BYTE times);

//====================================<Function.c headers>==============================================================

//========================= ��������� ==================================================================================

#if defined(__CC_ARM)
#pragma anon_unions
#endif


typedef union _Byte
{
	BYTE _byte;
	struct
	{
		unsigned b0 : 1;
		unsigned b1 : 1;
		unsigned b2 : 1;
		unsigned b3 : 1;
		unsigned b4 : 1;
		unsigned b5 : 1;
		unsigned b6 : 1;
		unsigned b7 : 1;
	};
} Byte;


typedef union _Word
{
	WORD _word;
	BYTE v[2];
	struct
	{
		unsigned b0 : 1;
		unsigned b1 : 1;
		unsigned b2 : 1;
		unsigned b3 : 1;
		unsigned b4 : 1;
		unsigned b5 : 1;
		unsigned b6 : 1;
		unsigned b7 : 1;
		unsigned b8 : 1;
		unsigned b9 : 1;
		unsigned b10 : 1;
		unsigned b11 : 1;
		unsigned b12 : 1;
		unsigned b13 : 1;
		unsigned b14 : 1;
		unsigned b15 : 1;
	};
} Word;
typedef union _Dword
{
	DWORD _dword;
	BYTE v[4];
	struct
	{
		unsigned b0 : 1;
		unsigned b1 : 1;
		unsigned b2 : 1;
		unsigned b3 : 1;
		unsigned b4 : 1;
		unsigned b5 : 1;
		unsigned b6 : 1;
		unsigned b7 : 1;
		unsigned b8 : 1;
		unsigned b9 : 1;
		unsigned b10 : 1;
		unsigned b11 : 1;
		unsigned b12 : 1;
		unsigned b13 : 1;
		unsigned b14 : 1;
		unsigned b15 : 1;
		unsigned b16 : 1;
		unsigned b17 : 1;
		unsigned b18 : 1;
		unsigned b19 : 1;
		unsigned b20 : 1;
		unsigned b21 : 1;
		unsigned b22 : 1;
		unsigned b23 : 1;
		unsigned b24 : 1;
		unsigned b25 : 1;
		unsigned b26 : 1;
		unsigned b27 : 1;
		unsigned b28 : 1;
		unsigned b29 : 1;
		unsigned b30 : 1;
		unsigned b31 : 1;
	};
} Dword;

typedef struct
{
	BYTE  YY;
	BYTE  MM;
	BYTE  DD;
	BYTE  HH;
	BYTE  MIN;
	BYTE  SS;
	WORD  ZZZ;
}TimeTypeDef;
//============================== ���������� ============================================================================
//Dword *ODRA;
//Dword *ODRB;
//Dword *ODRC;
//Dword *ODRD;
//Dword *ODRE;
//Dword *ODRF;
//Dword *ODRG;
//Dword *ODRI;
//Dword *ODRH;
extern Dword *LATA;
extern Dword *LATB;
extern Dword *LATC;
extern Dword *LATD;
extern Dword *LATE;
extern Dword *LATF;
extern Dword *LATG;
extern Dword *LATI;
extern Dword *LATH;

extern Dword *PORTA;
extern Dword *PORTB;
extern Dword *PORTC;
extern Dword *PORTD;
extern Dword *PORTE;
extern Dword *PORTF;
extern Dword *PORTG;
extern Dword *PORTI;
extern Dword *PORTH;
//======================================================================================================================
#define R0_0                GPIO_PIN_0                 /* Pin 0 selected    */
#define R1_0                GPIO_PIN_1                 /* Pin 1 selected    */
#define R2_0                GPIO_PIN_2                 /* Pin 2 selected    */
#define R3_0                GPIO_PIN_3                 /* Pin 3 selected    */
#define R4_0                GPIO_PIN_4                 /* Pin 4 selected    */
#define R5_0                GPIO_PIN_5                 /* Pin 5 selected    */
#define R6_0                GPIO_PIN_6                 /* Pin 6 selected    */
#define R7_0                GPIO_PIN_7                 /* Pin 7 selected    */
#define R8_0                GPIO_PIN_8                 /* Pin 8 selected    */
#define R9_0                GPIO_PIN_9                 /* Pin 9 selected    */
#define R10_0               GPIO_PIN_10                /* Pin 10 selected   */
#define R11_0               GPIO_PIN_11                /* Pin 11 selected   */
#define R12_0               GPIO_PIN_12                /* Pin 12 selected   */
#define R13_0               GPIO_PIN_13                /* Pin 13 selected   */
#define R14_0               GPIO_PIN_14                /* Pin 14 selected   */
#define R15_0               GPIO_PIN_15                /* Pin 15 selected   */

#define R0_1                (uint32_t)GPIO_PIN_0<<16    /* Pin 0 selected    */
#define R1_1                (uint32_t)GPIO_PIN_1<<16    /* Pin 1 selected    */
#define R2_1                (uint32_t)GPIO_PIN_2<<16    /* Pin 2 selected    */
#define R3_1                (uint32_t)GPIO_PIN_3<<16    /* Pin 3 selected    */
#define R4_1                (uint32_t)GPIO_PIN_4<<16    /* Pin 4 selected    */
#define R5_1                (uint32_t)GPIO_PIN_5<<16    /* Pin 5 selected    */
#define R6_1                (uint32_t)GPIO_PIN_6<<16    /* Pin 6 selected    */
#define R7_1                (uint32_t)GPIO_PIN_7<<16    /* Pin 7 selected    */
#define R8_1                (uint32_t)GPIO_PIN_8<<16    /* Pin 8 selected    */
#define R9_1                (uint32_t)GPIO_PIN_9<<16    /* Pin 9 selected    */
#define R10_1               (uint32_t)GPIO_PIN_10<<16   /* Pin 10 selected   */
#define R11_1               (uint32_t)GPIO_PIN_11<<16   /* Pin 11 selected   */
#define R12_1               (uint32_t)GPIO_PIN_12<<16   /* Pin 12 selected   */
#define R13_1               (uint32_t)GPIO_PIN_13<<16   /* Pin 13 selected   */
#define R14_1               (uint32_t)GPIO_PIN_14<<16   /* Pin 14 selected   */
#define R15_1               (uint32_t)GPIO_PIN_15<<16   /* Pin 15 selected   */
//======================================================================================================================
struct gpio_s {
	unsigned int crl;
	unsigned int crh;
	unsigned int idr;
	unsigned int odr;
	unsigned int bsrr;
	unsigned int brr;
	unsigned int lckr;
}
;

struct gpio_us {
	union {
		struct gpio_s raw;
		struct _gpio_b {
			struct _crl {
				unsigned int mode0 : 2;
				unsigned int cnf0 : 2;
				unsigned int mode1 : 2;
				unsigned int cnf1 : 2;
				unsigned int mode2 : 2;
				unsigned int cnf2 : 2;
				unsigned int mode3 : 2;
				unsigned int cnf3 : 2;
				unsigned int mode4 : 2;
				unsigned int cnf4 : 2;
				unsigned int mode5 : 2;
				unsigned int cnf5 : 2;
				unsigned int mode6 : 2;
				unsigned int cnf6 : 2;
				unsigned int mode7 : 2;
				unsigned int cnf7 : 2;
			} crl;
			struct _crh {
				unsigned int mode8 : 2;
				unsigned int cnf8 : 2;
				unsigned int mode9 : 2;
				unsigned int cnf9 : 2;
				unsigned int mode10 : 2;
				unsigned int cnf10 : 2;
				unsigned int mode11 : 2;
				unsigned int cnf11 : 2;
				unsigned int mode12 : 2;
				unsigned int cnf12 : 2;
				unsigned int mode13 : 2;
				unsigned int cnf13 : 2;
				unsigned int mode14 : 2;
				unsigned int cnf14 : 2;
				unsigned int mode15 : 2;
				unsigned int cnf15 : 2;
			} crh;		
			unsigned int idr;
			unsigned int odr;
			unsigned int bsrr;
			unsigned int brr;
			unsigned int lckr;
		} b;
	};
};

#define gpioa ((struct gpio_us*)(GPIOA_BASE))
#define gpiob ((struct gpio_us*)(GPIOB_BASE))
#define gpioc ((struct gpio_us*)(GPIOC_BASE))
#define gpiod ((struct gpio_us*)(GPIOD_BASE))
//======================================================================================================================

#endif // !__AKBARDEFF_H
